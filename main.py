__author__ = 'JackSong'
import sys
import os.path

import tornado.ioloop
import tornado.web
import tornado.httpserver
import tornado.options
import logging
import logging.handlers
from tornado.options import options

from config import init_config
from urls import handlers
from ioloop import IOloop

reload(sys)
sys.setdefaultencoding('utf-8')
settings = dict(
    template_path=os.path.join(os.path.dirname(__file__), "templates"),
    static_path=os.path.join(os.path.dirname(__file__), "static"),
)


class Application(tornado.web.Application):
    def __init__(self):
        tornado.web.Application.__init__(self, handlers, **settings)


def main():
    init_config()
    options.parse_config_file("openssh.conf")
    options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    IOloop.instance()
    logger = logging.getLogger()
    timelog = logging.handlers.TimedRotatingFileHandler(options.log_file_prefix)
    logger.addHandler(timelog)
    print('Server Starting port on %s' % (options.port,))
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
